'''
Created on 10-Mar-2018

@author: shobhasrao
'''
#APPLICATION  LOG FILES 
LOG_FILENAME = "output/logs/cervastra.log"
DATA_BACKEND = 'datastore'
PROJECT_ID = 'devcervastra-221109'

DATASTORE_SERVICE_ACCOUNT_JSON = 'devcervastra-221109-6380da432140.json'
#DATASTORE_SERVICE_ACCOUNT_JSON = 'testcervastra-219018-5f47adcea9bf.json'

# You can adjust the max content length and allow extensions settings to allow
# larger or more varied file types if desired.
#CLOUD_STORAGE_BUCKET = 'your-bucket-name'
#MAX_CONTENT_LENGTH = 8 * 1024 * 1024
#ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
