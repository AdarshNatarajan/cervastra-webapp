'''
Created on 15-Mar-2018
@author: shobhasrao
'''
from protorpc import messages

class AstraSubscriptionMessage(messages.Message):
    subscriptionID = messages.IntegerField(1, required=True)
    subscriptionName = messages.StringField(2, required=True)
    subscriptionPriceINR = messages.IntegerField(3, required=True)
    subscriptionAllottedRunsCount = messages.IntegerField(4, required=True)
    subscriptionValidityInDays = messages.IntegerField(5, required=True)
    
class AstraDeviceMessage(messages.Message): 
    deviceID = messages.StringField(1,required=True)
    modelNumber = messages.StringField(2,required=True)
    configuredDate = messages.StringField(3,required=True)
    licenseKey = messages.StringField(4,required=False)
    licenseSubscriptionID = messages.StringField(5,required=False)
    licenseRenewed = messages.BooleanField(6,required=False) 
    licenseRenewedDate = messages.StringField(7,required=False)
    accessToken = messages.StringField(8,required=False) #decoded access Token
    secretKey = messages.StringField(9,required=False) 
    status = messages.StringField(10, required=False)

class AstraUserMessage(messages.Message):
    astraUserType = messages.StringField(1,required=True)
    astraUserEmailAddress = messages.StringField(2,required=True)
    astraUserFirstName = messages.StringField(3,required=True)
    astraUserLastName = messages.StringField(4,required=False)
    dateOfBirth = messages.StringField(5,required=False)
    gender = messages.StringField(6,required=False)
    contactNumber = messages.StringField(7,required=False)
    address = messages.StringField(8, required=False)
    pinCode = messages.IntegerField(9, required=False)
    cityName = messages.StringField(10,required=False)
    stateName = messages.StringField(11, required=False)
    countryName = messages.StringField(12,required=False)
    ##OtherUser
    yourRequirements = messages.StringField(13, required=False)
    ##NGOUser
    ngoRegistrationNumber = messages.StringField(14, required=False)
    labnameOrHospitalname = messages.StringField(15,required=False)
    ##PathologistUser
    isHeadOfPathology = messages.BooleanField(16, required=False)
    mciNumber = messages.StringField(17, required=False)
    yearsOfPractice = messages.IntegerField(18,required=False)

class PatientClinicalDataMessage(messages.Message): 
    height_in_inches = messages.FloatField(1, required=True)
    weight_in_kgs = messages.FloatField(2, required=True)
    bloodGroup = messages.StringField(3, required=True)

class ReportHeaderMessage(messages.Message):
    date_of_receipt = messages.StringField(1, required=False)
    paid_receipt_number = messages.StringField(2, required=False)
    cytology_slide_number = messages.StringField(3, required=True)
    registration_number = messages.StringField(4, required=False)

class StainingQuality(messages.Enum):
    GOOD = 1
    SATISFACTORY = 2
    UNSATISFACTORY = 3
    
class SampleAdequacy(messages.Enum):
    ADEQUATE =1
    INADEQUATE =2 
    LIMITED_BY_ABSENCE_OF_ENDOCERVICAL_CELLS=3 
    LIMITED_BY_ABSENCE_OF_ECTOCERVICAL_CELLS=4
    
class ReportContentMessage(messages.Message):
    report_id = messages.StringField(1, required=True) #user pathologistfullname here
    quality_of_staining = messages.EnumField(StainingQuality, 2, required=False)
    quality_of_staining_comments = messages.StringField(3, required=False)
    sample_adequacy = messages.EnumField(SampleAdequacy, 4, required=False)
    infection_associated = messages.StringField(5, required=False)
    reactivechanges_associated = messages.StringField(6, required=False)
    squamous_ASCUS = messages.StringField(7, required=False)
    squamous_LSIL = messages.StringField(8, required=False)
    squamous_HSIL = messages.StringField(9, required=False)
    squamous_invasive = messages.StringField(10, required=False)
    glandular_endometrail = messages.StringField(11, required=False)
    glandular_AGUS = messages.StringField(12, required=False)
    glandular_adenocarcinoma = messages.StringField(13, required=False)
    remarks = messages.StringField(14, required=False)
            
class CasesMessage(messages.Message):
    deviceID = messages.StringField(1, required=True)
    caseID = messages.StringField(2, required=True)
    patientFirstName = messages.StringField(3, required=True)
    patientLastName = messages.StringField(4, required=True)
    patientFullName = messages.StringField(5, required=False)
    patientDOB = messages.StringField(6, required=True)
    patientAddress = messages.StringField(7, required=True)
    patientContactNumber = messages.StringField(8, required=True)
    villageName = messages.StringField(9, required=False)
    talukName = messages.StringField(10, required=False)
    districtName = messages.StringField(11, required=False)
    cityName = messages.StringField(12, required=False) 
    stateName = messages.StringField(13, required=True)
    countryName = messages.StringField(14, required=False)
    patientClinicalDataMsg = messages.MessageField(PatientClinicalDataMessage,15, required=False)
    patientClinicalHistory = messages.StringField(16, required=True)
    additionalReportHeaderData = messages.MessageField(ReportHeaderMessage, 17, required=False)
    caseAnnotationMetaData = messages.StringField(18, required=False)
    updatedAnnotationMetaData = messages.StringField(19, required=False, repeated=True)
    caseImageURNPath = messages.StringField(20, required=False)
    caseReports = messages.MessageField(ReportContentMessage,21, required=False, repeated=True)

class AstraGetResponseMessage(messages.Message): 
    deviceStatus = messages.StringField(1,required=True)   
    
class AstraIDResponseMessage(messages.Message):
    id = messages.StringField(1, required=True)
    
class AstraUserResponseMessage(messages.Message):
    id = messages.StringField(1, required=True)
    astraUserFullName = messages.StringField(2, required=True)
    allocatedCases = messages.StringField(3, repeated=True, required=False)
    completedCases = messages.StringField(4, repeated=True, required=False)

class AllAstraUsersResponseMessage(messages.Message):
    id = messages.StringField(1, required=True)
    astraUserFullName = messages.StringField(2, required=True)
    userVerfiedStatus = messages.BooleanField(3,required=True)
    userActiveStatus = messages.BooleanField(4,required=True)
    #astraUserType = messages.StringField(5,required=True)
    
class AstraReportRequestMessage(messages.Message):
    device_id = messages.StringField(1, required=True)
    timeOfLastClosedCase = messages.StringField(2,required=False)
    #list_of_case_ids = messages.StringField(2, required=True, repeated=True)
    
class AstraReportResponseMessage(messages.Message):
    deviceid = messages.StringField(1, required=True)
    closedCaseIDs = messages.StringField(2, required=False, repeated=True)
    closedCaseReports = messages.MessageField(ReportContentMessage,3, required=False, repeated=True)
            
class AstraCaseUpdatedMessage(messages.Message):
    updatedSingleAnnotationMetaData = messages.StringField(1, required=False)
    #updatedSingleAnnotationMetaData = messages.MessageField(AstraCaseAnnotationMetaData,1, repeated=True, required=False)
    caseSingleReport = messages.MessageField(ReportContentMessage,2, required=False)
    caseStatus = messages.IntegerField(3, required=True)
    referralPathologistEmailAddress = messages.StringField(4,required=False)
    
