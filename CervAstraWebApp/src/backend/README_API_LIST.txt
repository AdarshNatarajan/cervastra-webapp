

[2] Called after firebase authentication by Web Portal Client to check for user exist
	# localhost:8080/users?astrauser_id="" ==> GET ==> lookup_user_exists() 
	# If user exists in our System, then return appropriate information
	# like allocated cases for Pathologist
	# OR just user full Name for NGO and Other User types
	#  http://localhost:8080/_ah/api/cervastraApi/v1/users?astrauser_id=kvsandhya4@gmail.com
	
[3] Register of User - Invoked on Webportal when User selects Sign Up 
	# localhost:8080/users ==> POST  ==> insert_astrauser()
	#  http://localhost:8080/_ah/api/cervastraApi/v1/users

[4]  Configuring a new VisionX Device - Invoked by Admin from Admin Web Portal
	# localhost:8080/devices ==> POST ==> insert_astradevice()
	# http://localhost:8080/_ah/api/cervastraApi/v1/devices


	
[6] Fetch Case Details when each Case ID Link is Clicked on Web Portal by Pathologist
	# localhost:8080/cases/{astracase_id} ==> GET ==> fetch_case_details()
	# http://localhost:8080/_ah/api/cervastraApi/v1/cases/VX12AB457-1002
	
[7] Upload the changed/modified Case Details (changed by Pathologist), 
    invoked from Web Portal
    # This includes upload of metadata/annotations + report data
    ## localhost:8080/cases/{astracase_id} ==> POST ==> submit_astracasereport()
    #  http://localhost:8080/_ah/api/cervastraApi/v1/cases/VX12AB457-1002

[9] Verifying and Activating a Registered User 
    # - Invoked by Admin from Admin Web Portal
	# localhost:8080/users ==> POST ==> activate_user()
	# http://localhost:8080/_ah/api/cervastraApi/v1/users/{astrauser_id} ==> POST ==> activate_astrauser()
	
[10] Listing all Registered Users with Type and Activation Status 
     #  - Invoked by Admin from Admin Web Portal
	 # localhost:8080/users ==> GET  ==> get_all_users()
	 #  http://localhost:8080/_ah/api/cervastraApi/v1/users/all ==> GET ==> fetch_all_users

[5] Invoked when the Configure Button is Clicked on VisionX Device UI
	# localhost:8080/devices/{device_id} ==> GET ==> configure_astradevice() 
	#  http://localhost:8080/_ah/api/cervastraApi/v1/devices/VX12AB457
	
[1] Upload the case and patient metadata  -Invoked from VisionX Client
	# localhost:8080/cases ==> POST  ==> upload_astracase() 
	# reportheader and clinicaldatamsg is MANDATORY, caseReports is optional
	#  http://localhost:8080/_ah/api/cervastraApi/v1/cases
		 
[8] Download the reports from server to VisionX device, invoked by VisionX Client
    # Vision X device sends a list of caseIDs which are awaiting reports
    # Server fetches report Header details and report details for each case
    # and sends the same as an array of reports
    # for every case only one report will be sent to VisionX per case
    # http://localhost:8080/_ah/api/cervastraApi/v1/cases?device_id=VX12AB457&lastTime=2018-04-07 
    #				==> GET ==> fetch_closed_reports()

[11] When VisionX goes online, it identifies itself with a HEAD request
    # with parameters of device ID, license ID and hashed access token,
    # Server uses secret key to unhash the access token and compares
    # with data in Datastore; if device is authentic, server sends a
    # response with a meta-data in header confirming "OK"
    
 