'''
Created on 07-Apr-2018

@author: shobhasrao
'''
import enum
from google.appengine.api import mail
from google.appengine.api import app_identity
import os

class CaseStatus(enum.Enum):
    NEW = 1
    OPEN = 2
    REFERRED = 3
    REFERRED_CLOSE = 4
    CLOSED = 5

def detect_GAE_environment():
    server_software = os.environ.get('SERVER_SOFTWARE', '')
    if server_software.startswith('Development/'):
        return False
    elif server_software.startswith('Google App Engine/'):
        return True
    
def send_approved_mail(toName, toAddress):
    if detect_GAE_environment():
        sender_address = '{}@appspot.gserviceaccount.com'.format(app_identity.get_application_id())
    else:
        sender_address ='appsupport@aindra.in'
    message = mail.EmailMessage(
    sender=sender_address,
    subject="Your account has been approved")
        #message.to = "Albert Johnson <Albert.Johnson@example.com>"
    message.to = toName + " " + toAddress
    message.body = """Your account has been approved.  You can now visit
            http://www.cervastra.in/ and sign in using your Account.
            Please let us know if you have any questions.
            Aindra Systems Team"""
    try:        
        message.send()
    except google.appengine.api.mail_errors.Error as e:
        logging.error('send_approved_mail Failure for EmailSender %s ,'
                      'failed with exception %s' % (to, e))

    
def send_case_notify_mail(toAddress):
    if detect_GAE_environment():
        sender_address = '{}@appspot.gserviceaccount.com'.format(app_identity.get_application_id())
    else:
        sender_address ='appsupport@aindra.in'
    message = mail.EmailMessage(
    sender=sender_address,
    subject="Case Allocated to Your Account")
        #message.to = "Albert Johnson <Albert.Johnson@example.com>"
    message.to = toAddress
    message.body = """Case/(s) has been allocated to your account. Visit
            http://www.cervastra.in/ and sign in to your Account to review Case/(s).
            Please let us know if you have any questions.
            Aindra Systems Team"""
    try:        
        message.send()
    except google.appengine.api.mail_errors.Error as e:
        logging.error('send_case_notify_mail Failure for EmailSender %s ,'
                      'failed with exception %s' % (to, e))
        
def send_case_referral_mail(toAddress):
    if detect_GAE_environment():
        sender_address = '{}@appspot.gserviceaccount.com'.format(app_identity.get_application_id())
    else:
        sender_address ='appsupport@aindra.in'
    message = mail.EmailMessage(
    sender=sender_address,
    subject="Case Referred to Your Account")
        #message.to = "Albert Johnson <Albert.Johnson@example.com>"
    message.to = toAddress
    message.body = """Case/(s) has been Referred to You for your review. Visit
            http://www.cervastra.in/ and sign in to your Account to review Case/(s).
            Please let us know if you have any questions.
            Aindra Systems Team"""
            
    try:
        message.send()
    except google.appengine.api.mail_errors.Error as e:
        logging.error('send_case_referral_mail Failure for EmailSender %s ,'
                      'failed with exception %s' % (to, e))  

def return_query_page(query_class, size=10, bookmark=None, is_prev=None, equality_filters=None, orders=None):
    """
    Generate a paginated result on any class
    Param query_class: The ndb model class to query
    Param size: The size of the results
    Param bookmark: The urlsafe cursor of the previous queris. First time will be None
    Param is_prev: If your requesting for a next result or the previous ones
    Param equal_filters: a dictionary of {'property': value} to apply equality filters only
    Param orders: a dictionary of {'property': '-' or ''} to order the results like .order(cls.property)
    Return: a tuple (list of results, Previous cursor bookmark, Next cursor bookmark)
    """
    if bookmark:
        cursor = ndb.Cursor(urlsafe=bookmark)
    else:
        is_prev = None
        cursor = None
    
    equality_filters = equality_filters or {}
    orders = orders or {}
    
    q = query_class.query()
    try:
        for prop, value in equality_filters.iteritems():
            q = q.filter(getattr(query_class, prop) == value)

        q_forward = q.filter()
        q_reverse = q.filter()

        for prop, value in orders.iteritems():
            if value == '-':
                q_forward = q_forward.order(-getattr(query_class, prop))
                q_reverse = q_reverse.order(getattr(query_class, prop))
            else:
                q_forward = q_forward.order(getattr(query_class, prop))
                q_reverse = q_reverse.order(-getattr(query_class, prop))
    except:
        return None, None, None
    if is_prev:
        qry = q_reverse
        new_cursor = cursor.reversed() if cursor else None
    else:
        qry = q_forward
        new_cursor = cursor if cursor else None

    results, new_cursor, more = qry.fetch_page(size, start_cursor=new_cursor)
    if more and new_cursor:
        more = True
    else:
        more = False

    if is_prev:
        prev_bookmark = new_cursor.reversed().urlsafe() if more else None
        next_bookmark = bookmark
        results.reverse()
    else:
        prev_bookmark = bookmark
        next_bookmark = new_cursor.urlsafe() if more else None

    return results, prev_bookmark, next_bookmark       

# usually this function is included as a class method inside a Model so cls is always the class calling
#    results, prev_b, next_b = return_query_page(Note, 1, None, None, {'published': True}, {'creation_date': '-'})
  
    #calling next page
#    results, prev_b, next_b = return_query_page(Note, 1, next_b, None, {'published': True}, {'creation_date': '-'})
   
    #calling prev page
#    results, prev_b, next_b = return_query_page(Note, 1, prev_b, True, {'published': True}, {'creation_date': '-'})
