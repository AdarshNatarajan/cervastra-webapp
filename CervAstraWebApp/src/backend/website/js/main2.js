var ready = function(fn) {
if (document.readyState != 'loading') {
fn();
} else {
document.addEventListener('DOMContentLoaded', fn);
}
};

var checkLoad = function() {
if (this.status >= 200 && this.status < 400) {
document.getElementById(‘hipchat’).style.display = ‘none’;
document.getElementById(‘confirm’).style.display = ‘block’;
} else {
alert(‘Error with API Endpoint\n’ + this.response);
}
};

var ajaxError = function() {
alert(‘Connection Error with API Endpoint’);
};

var processFormData = function() {
var formData = ‘Name: ‘ + document.getElementById(‘name’).value +
‘ | Email: ‘ + document.getElementById(’email’).value +
‘ | Message: ‘ + document.getElementById(‘message’).value;

return JSON.stringify({
color: ‘green’,
message: formData,
notify: ‘true’
});
};

var submitter = function(event) {
var request = new XMLHttpRequest();
var room = ‘1753375’;

request.open(‘POST’, ‘/hipchat/’ + room, true);

request.setRequestHeader(
‘Content-Type’,
‘application/json; charset=UTF-8’
);

request.setRequestHeader(
‘X-Requested-With’,
‘XMLHttpRequest’
);

request.onload = checkLoad;
request.onerror = ajaxError;
request.send(processFormData());
event.preventDefault();
};

ready(function() {
document.getElementById(‘hipchat’)
.addEventListener(‘submit’, submitter);
});

