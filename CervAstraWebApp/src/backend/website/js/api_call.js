function invokeService() {
    $(document).ready(function() {
          $.ajax({
            type: "GET",
            async: false,
            url: "https://inf:7871/smcfs/heartbeat",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: true,
            success: function(result) {
              AjaxSucceeded(result);
            },
            eror: AjaxFailed
          });

          function AjaxSucceeded(result) {
            alert(result);
          }

          function AjaxFailed(result) {
            alert(result.status + '' + result.statusText);
          }
        };
