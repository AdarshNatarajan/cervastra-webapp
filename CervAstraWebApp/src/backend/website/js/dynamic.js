//Workout Form    
$(document).ready(function () {
    $("#date_added").change(function () {
        var dob = $('#date_added').val();
        var dob = new Date(dob);
        var currentYear = new Date();
        var age = Math.floor((currentYear - dob) / (365.25 * 24 * 60 * 60 * 1000));
        $("#age1").val(age);
        $("#age2").val(age);
    });
    $('.gender').click(function () {
        var getval = $(this).children().find("input").val();
        $('#sex').val(getval);
    });
    $('.clevel').click(function () {
        var getval = $(this).children().find("input").val();
        $('#clavel_val').val(getval);
    });
    $('.slevel').click(function () {
        var getval = $(this).children().find("input").val();
        $('#slavel_val').val(getval);
    });

    $('.test').click(function () {
        var getval = $(this).children().find("input").val();
        $('#occupation_val').val(getval);
    });
});
$('body').on('click', '.remove', function () {
        $(this).parent().parent().remove();
    });
    //Workout Form End
    //----------------------------------------------------------------


//Create Table Start
var counter = 0;
var rowid = 0;
$('#create').click(function (e) {
    e.preventDefault();

    var workout_name = $('#workout_name').val();
    $("#workouthidden").attr('value', workout_name); //Assigning value to hidden workout name

    var comment_get = $('#comment_get').val();
    $("#commenthidden").attr('value', comment_get); //Assigning value to hidden workout name

    var workout_category = $('#workout_category').val();

    var checkboxvalue = $('input[type=checkbox]:checked').map(function (_, el) {
        return $(el).val();
    }).get(); //Getting all checkbox values

    if (workout_name === '') {
        alert("Please Enter Workout Name");
    } else if (workout_category === '') {
        alert("Please Enter Category Name");
    } else if (checkboxvalue.length < 1) {
        alert("Please Enter Workout Days");
    } else {

        counter++;
        //Create submit button if counter == 1 
        if (counter == 1) {
            var hrline = document.createElement('div');
            hrline.setAttribute('class', 'hr-line-dashed float-align');
            $('.workout-submit').append($(hrline));

            var submitDiv = document.createElement('div');
            submitDiv.setAttribute('class', 'col-sm-offset-10 col-sm-2');
            submitDiv.innerHTML = '<input type="submit" name="Submit" class="btn btn-primary">';
            $('.workout-submit').append($(submitDiv));
        } else {}
        //Create submit Ends

        var workoutcat = 'workoutcategory' + counter;
        var newWorkout = document.createElement('div');
        newWorkout.setAttribute('class', 'workout float-align');
        newWorkout.setAttribute('id', workoutcat);
        $('.workout-main').append($(newWorkout));

        var workoutHead = document.createElement('h2');
        workoutHead.setAttribute('class', 'float-align');
        workoutHead.innerHTML = workout_category + ' <i>' + 'Days: ' + checkboxvalue  + '</i>' + '<i class="fa fa-times remove" aria-hidden="true"></i>';
        $(newWorkout).append($(workoutHead));


        var formGroup = document.createElement('div');
        formGroup.setAttribute('class', 'form-group');
        $(newWorkout).append($(formGroup));


        var column = document.createElement('div');
        column.setAttribute('class', 'col-sm-12');
        $(formGroup).append($(column));

        var table = document.createElement('table');
        table.setAttribute('class', 'table table-bordered align-center');
        $(column).append($(table));

        var thead = document.createElement('thead');
        $(table).append($(thead));

        var thead_tr = document.createElement('tr');
        thead_tr.innerHTML = '<th>Exercise</th><th>Sets <!--(Duration in "min" for Cardio)--></th><th>Reps <!--(Intensity for Cardio)--></th><th>Action</th>';
        $(thead).append($(thead_tr));


        var tbodyid = 'tbody' + counter;

        var tbody = document.createElement('tbody');
        tbody.setAttribute('id', tbodyid);
        $(table).append($(tbody));
        var aaa = 104;
        var tbody_tr = document.createElement('tr');


        var workoutCategoryDays = checkboxvalue + ' Days'.toString();
        tbody_tr.innerHTML = '<input type="hidden" name="wc" value="' + workout_category + '" class="wc"/><input type="hidden" name="wd" value="' + workoutCategoryDays + '" class="wd"/><td><select name="exercise" class="form-control chosen-select down" required><option value="" selected disabled>Select and exercise</option></select></td><td><input type="text" name="sets" class="form-control" required></td><td><input type="text" class="form-control" name="reps" required></td><td><button class="btn btn-primary btn-circle" type="button" onclick="createRow(' + counter + ');"><i class="fa fa-plus" aria-hidden="true"></i></button></td>';
        $('#' + tbodyid).append($(tbody_tr));

        rowid++;



        $('#workout_category').prop('selectedIndex', 0); // Reset Category
        $('.ch').attr('checked', false); // Unchecks Checkbox
    }

    $.ajax({
        type: 'POST',
        url: baseURL + 'common/workout-library',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (index, element) {
                $('.down').append('<option value="' + data[index].text + '">' + data[index].text + '</option>');
            });
            var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
            }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
        }
    })
});

function createRow(counter) {
    var tbodyid = 'tbody' + counter;
    var tbody_tr = document.createElement('tr');
    tbody_tr.innerHTML = '<td><select name="exercise' +
        '" class="form-control down chosen-select" required><option value="" selected disabled>Select and exercise</option></select></td><td><input type="text" class="form-control" name="sets" required></td><td><input name="reps" type="text" class="form-control" required></td><td><button class="btn btn-danger btn-circle " type="button" onclick="deleteRow(this)"><i class="fa fa-minus" aria-hidden="true"></i></button></td>';
    $('#' + tbodyid).append($(tbody_tr));

    rowid++;
    $.ajax({
        type: 'POST',
        url: baseURL + 'common/workout-library',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (index, element) {
                $('.down').append('<option value="' + data[index].text + '">' + data[index].text + '</option>');
            });
            var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
            }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
        }
    })

}

function deleteRow(btn) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
}