function uploadcase()
{
var data = JSON.stringify({
  "deviceID": "VX12AB457",
  "caseID": 1001,
  "patientFirstName": "User1",
  "patientLastName": "Surname1",
  "patientDOB": "1980-03-15",
  "patientAddress": "No 22, 1st Cross, Kerebhaavi",
  "patientContactNumber": "9123000789",
  "villageName": "Thirthalli",
  "talukName": "Tumkur",
  "districtName": "Tumkur",
  "cityName": "",
  "stateName": "Karnataka",
  "countryName": "INDIA",
  "patientClinicalDataMsg": {
    "height_in_inches": 5.3,
    "weight_in_kgs": 76.3,
    "bloodGroup": "O+ve"
  },
  "patientClinicalHistory": "History of constant abdominal pains",
  "additionalReportHeaderData": [
    {
      "date_of_receipt": "2018-01-01",
      "paid_receipt_number": "ABC-RECEIPT101",
      "cytology_slide_number": "1001",
      "registration_number": "ABC-REG101"
    }
  ],
  "caseImageURNPath": "https://console.cloud.google.com/storage/browser/testcervastra-219018.appspot.com",
  "caseAnnotationMetaData": "LSIL"
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4) {
    console.log(this.responseText);
  }
});

var response=xhr.open("POST", "https://testcervastra-219018.appspot.com/_ah/api/cervastraApi/v1/cases");
xhr.setRequestHeader("Content-Type", "application/json");
xhr.setRequestHeader("cache-control", "no-cache");

xhr.send(data);
}