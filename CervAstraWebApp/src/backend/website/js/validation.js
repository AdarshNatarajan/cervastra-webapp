function formValidation()
{
    var fname = document.registration.FirstName;
    var lname = document.registration.LastName;
    var email = document.registration.Email;
    if(allLetter(fname,lname))
    {
        if(ValidateEmail(email))
        {
                alert('Form Succesfully Submitted');
                window.location.reload()
                return true;
        } 
    }
    return false;
}
function allLetter(fname,lname)
{ 
    var letters = /^[A-Za-z]+$/;
    if(fname.value.match(letters) && lname.value.match(letters)) 
    {
        return true;
    }
    else
    {
        alert('Username must have alphabet characters only');
        fname.focus();
        return false;
    }
}
function ValidateEmail(email)
{
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(email.value.match(mailformat))
    {
        return true;
    }
    else
    {
        alert("You have entered an invalid email address!");
        return false;
    }
} 
