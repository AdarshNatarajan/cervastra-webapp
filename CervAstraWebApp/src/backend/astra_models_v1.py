'''
Created on 15-Mar-2018

@author: shobhasrao
'''
from google.appengine.ext import ndb
from google.appengine.datastore.datastore_query import Cursor
from google.appengine.ext.ndb import msgprop
from message_models_v1 import PatientClinicalDataMessage, ReportHeaderMessage, ReportContentMessage

ITEMS_PER_PAGE=20

class AstraSubscription(ndb.Model):
    subscriptionID = ndb.IntegerProperty(indexed=True)
    subscriptionName = ndb.StringProperty(indexed=False)
    subscriptionPriceINR = ndb.IntegerProperty(indexed=False)
    subscriptionAllottedRunsCount = ndb.IntegerProperty(indexed=False)
    subscriptionValidityInDays = ndb.IntegerProperty(indexed=False)
    
class AstraDevice(ndb.Model):
    deviceID = ndb.StringProperty(indexed=True)
    modelNumber = ndb.StringProperty(indexed=False)
    configuredDate = ndb.DateProperty(indexed=False)
    licenseKey = ndb.StringProperty(indexed=False)
    licenseSubscriptionID = ndb.StringProperty(indexed=False)
    licenseRenewed = ndb.BooleanProperty(indexed=False) 
    licenseRenewedDate = ndb.DateProperty(indexed=False)
    accessToken = ndb.StringProperty(indexed=False) #decoded access Token
    secretKey = ndb.StringProperty(indexed=False) 
    status = ndb.BooleanProperty(indexed=False, default = False) 
    
    @classmethod
    def get_by_deviceid(cls, deviceid):
        return cls.query().filter(cls.deviceID == deviceid).get()
    
    def update_device_status(self):
        pass

         
class OtherUser(ndb.Model):
    astraUserEmailAddress = ndb.StringProperty(indexed=False)
    astraUserFirstName = ndb.StringProperty(indexed=False)
    astraUserLastName = ndb.StringProperty(indexed=False)
    astraUserFullName = ndb.StringProperty(indexed=False)
    dateOfBirth = ndb.DateProperty(indexed=False)
    gender = ndb.StringProperty(choices=('MALE', 'FEMALE', 'OTHERS'),indexed=False)
    contactNumber = ndb.StringProperty(indexed=False)
    address = ndb.StringProperty(indexed=False)
    pinCode = ndb.IntegerProperty(indexed=False)
    cityName = ndb.StringProperty(indexed=False)
    stateName = ndb.StringProperty(indexed=False)
    countryName = ndb.StringProperty(indexed=False)
    yourRequirements = ndb.StringProperty(indexed=False)
    aindraResponseStatus = ndb.BooleanProperty(indexed=True)

    def get_full_name(self, fname, lname): 
        full_name = fname+" " +lname; 
        return full_name; 

    def _pre_put_hook(self):
        self.userFullName = self.get_full_name(self.astraUserFirstName, self.astraUserLastName)
    
class NGOUser(ndb.Model):
    astraUserEmailAddress = ndb.StringProperty(indexed=False)
    astraUserFirstName = ndb.StringProperty(indexed=False)
    astraUserLastName = ndb.StringProperty(indexed=False)
    astraUserFullName = ndb.StringProperty(indexed=False)
    dateOfBirth = ndb.DateProperty(indexed=False)
    gender = ndb.StringProperty(choices=('MALE', 'FEMALE', 'OTHERS'),indexed=False)
    contactNumber = ndb.StringProperty(indexed=False)
    address = ndb.StringProperty(indexed=False)
    pinCode = ndb.IntegerProperty(indexed=False)
    cityName = ndb.StringProperty(indexed=False)
    stateName = ndb.StringProperty(indexed=False)
    countryName = ndb.StringProperty(indexed=False)
    ngoRegistrationNumber = ndb.StringProperty(indexed=False)
    labnameOrHospitalname = ndb.StringProperty(indexed=False)
    aindraResponseStatus = ndb.BooleanProperty(indexed=True)
    
    def get_full_name(self, fname, lname): 
        full_name = fname+" " +lname; 
        return full_name; 

    def _pre_put_hook(self):
        self.userFullName = self.get_full_name(self.astraUserFirstName, self.astraUserLastName)
    

''' When Google Datastore is sharded/ distributed, 
   the data within and entityGroup is always available together
   hence a good design if data consistency is your requirement.
   BEWARE: Entity Groups have the limitation of creating 
   datastore contention when you try to update them too rapidly
   The datastore will queue concurrent requests to wait their turn. 
   Requests waiting in the queue past the timeout period will throw 
   a concurrency exception. If you're expecting to update a single 
   entity or write to an entity group more than several times per second, 
   it's best to re-work your design early-on to avoid possible 
   contention once your application is deployed.
   Having Entity Groups are good for consistency, 
   they are bad for write throughput.'''
class PathologistUser(ndb.Model):
    astraUserEmailAddress = ndb.StringProperty(indexed=True)
    astraUserFirstName = ndb.StringProperty(indexed=False)
    astraUserLastName = ndb.StringProperty(indexed=False)
    astraUserFullName = ndb.StringProperty(indexed=False)
    dateOfBirth = ndb.DateProperty(indexed=False)
    gender = ndb.StringProperty(choices=('MALE', 'FEMALE', 'OTHERS'),indexed=False)
    contactNumber = ndb.StringProperty(indexed=False)
    address = ndb.StringProperty(indexed=False)
    pinCode = ndb.IntegerProperty(indexed=False)
    cityName = ndb.StringProperty(indexed=False)
    stateName = ndb.StringProperty(indexed=False)
    countryName = ndb.StringProperty(default='INDIA', indexed=False)
    isHeadOfPathology = ndb.BooleanProperty(indexed=True)
    mciNumber = ndb.StringProperty(indexed=False)
    yearsOfPractice = ndb.IntegerProperty(indexed=False)
    labnameOrHospitalname = ndb.StringProperty(indexed=True)
    userVerfiedStatus = ndb.BooleanProperty(default=False, indexed=False)
    userActiveStatus = ndb.BooleanProperty(default=False, indexed=True)
    ## The below are Cloud Datastore key
    #allocatedCases = ndb.KeyProperty(kind="Cases",repeated=True,indexed=True)
    #closedCases = ndb.KeyProperty(kind="Cases",repeated=True,indexed=False)
    
    def get_full_name(self, fname, lname): 
        full_name = fname+" " +lname; 
        return full_name; 

    def _pre_put_hook(self):
        self.userFullName = self.get_full_name(self.astraUserFirstName, self.astraUserLastName)
    
    def get_allocatedcases(self):
        return ndb.get_multi(self.allocatedCases)

    def get_closedcases(self):
        return ndb.get_multi(self.closedCases)

    @classmethod
    def cursor_pagination(cls, prev_cursor_str, next_cursor_str):
        if not prev_cursor_str and not next_cursor_str:
            objects, next_cursor, more = cls.query().fetch_page(ITEMS_PER_PAGE)
            prev_cursor_str = ''
            if next_cursor:
                next_cursor_str = next_cursor.urlsafe()
            else:
                next_cursor_str = ''
            next_ = True if more else False
            prev = False
        elif next_cursor_str:
            cursor = Cursor(urlsafe=next_cursor_str)
            objects, next_cursor, more = cls.query().order(cls.number).fetch_page(ITEMS_PER_PAGE, start_cursor=cursor)
            prev_cursor_str = next_cursor_str
            next_cursor_str = next_cursor.urlsafe()
            prev = True
            next_ = True if more else False
        elif prev_cursor_str:
            cursor = Cursor(urlsafe=prev_cursor_str)
            objects, next_cursor, more = cls.query().order(-cls.number).fetch_page(ITEMS_PER_PAGE, start_cursor=cursor)
            objects.reverse()
            next_cursor_str = prev_cursor_str
            prev_cursor_str = next_cursor.urlsafe()
            prev = True if more else False
            next_ = True

        return {'objects': objects, 'next_cursor': next_cursor_str, 'prev_cursor': prev_cursor_str, 'prev': prev,
                'next': next_}
 
#infection_associated = ndb.StringProperty(choices=('ADEQUATE', 'INADEQUATE', 
#    'LIMITED_BY_ABSENCE_OF_ENDOCERVICAL_CELLS', 'LIMITED_BY_ABSENCE_OF_ECTOCERVICAL_CELLS'))
#a. Inflammation b. Atropy c. Radiation d. IUCD e. Repair f. Any other (specify)
#reactivechanges_associated = ndb.StringProperty(choices=('Trichnomonas vaginalis', 'Coccbacilli', 
#    'Candida', 'Acinomyces', 'Herpes simples'))
    
      
class Cases(ndb.Model):
    deviceID = ndb.StringProperty(required=True,indexed=True)
    caseID = ndb.StringProperty(required=True,indexed=True)
    patientFirstName = ndb.StringProperty(required=True,indexed=False)
    patientLastName = ndb.StringProperty(required=True,indexed=False)
    patientFullName = ndb.StringProperty(indexed=False)
    patientDOB = ndb.DateProperty(required=True,indexed=False)
    patientAddress = ndb.StringProperty(indexed=False)
    patientContactNumber = ndb.StringProperty(indexed=False)
    villageName = ndb.StringProperty(indexed=False)
    talukName = ndb.StringProperty(indexed=False)
    districtName = ndb.StringProperty(indexed=False)
    cityName = ndb.StringProperty(indexed=False) 
    stateName = ndb.StringProperty(indexed=False)
    countryName = ndb.StringProperty(default='INDIA',indexed=False)
    patientClinicalData = msgprop.MessageProperty(PatientClinicalDataMessage)
    patientClinicalHistory = ndb.StringProperty(indexed=False)
    additionalReportHeaderData = msgprop.MessageProperty(ReportHeaderMessage) 
    #insurance, hospital ward
    caseAnnotationMetaData = ndb.StringProperty(indexed=False)
    caseImageURNPath = ndb.StringProperty(indexed=False)
    updatedAnnotationMetaData = ndb.StringProperty(repeated=True, indexed=False)
    caseReports = msgprop.MessageProperty(ReportContentMessage, repeated=True)
    caseStatus = ndb.IntegerProperty(choices=(1,2,3,4,5),default = 1,indexed=True)
    caseCreatedDate = ndb.DateProperty(auto_now_add=True, indexed=True)
    timeOfLastClosedCase = ndb.DateProperty(indexed=True)
    caseReferral = ndb.BooleanProperty(default=False, indexed=False)
    primaryCaseOwner = ndb.StringProperty(indexed=False)
    currentCaseOwner = ndb.StringProperty(indexed=False)
    
    def get_full_name(self, fname, lname): 
        full_name = fname+" " +lname; 
        return full_name; 

    def _pre_put_hook(self):
        self.patientFullName = self.get_full_name(self.patientFirstName, self.patientLastName)
   

class UserCaseOpenMapping(ndb.Model):
    astraUserEmailAddress = ndb.StringProperty(indexed=True)    
    allocatedCases = ndb.KeyProperty(kind="Cases")

class UserCaseClosedMapping(ndb.Model):
    astraUserEmailAddress = ndb.StringProperty(indexed=True) 
    closedCases = ndb.KeyProperty(kind="Cases")
