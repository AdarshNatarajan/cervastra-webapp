'''
Created on 15-Mar-2018

@author: shobhasrao
'''
import endpoints
from protorpc import remote
from protorpc import message_types
from protorpc import messages

from astra_models_v1 import Cases, AstraDevice
from astra_models_v1 import PathologistUser, NGOUser, OtherUser
from astra_models_v1 import UserCaseOpenMapping, UserCaseClosedMapping

from message_models_v1 import CasesMessage, AstraCaseUpdatedMessage
from message_models_v1 import AstraUserMessage, AstraDeviceMessage, AllAstraUsersResponseMessage
from message_models_v1 import AstraReportRequestMessage,AstraReportResponseMessage
from message_models_v1 import AstraIDResponseMessage, AstraUserResponseMessage  

import astrautils
from datetime import datetime, date
import logging
from astrautils import CaseStatus
from google.appengine.ext.db import Error
from google.appengine.ext import ndb
 
#from message_models_v1 import AstraGetResponseMessage
#from queries import *
#from backend.message_utils import GetResponse

EMPTY_STRING=""

@endpoints.api(name='cervastraApi', version='v1', description='CervAstra API')
class CervastraApi(remote.Service):

    # Invoked from VisionX Client to upload the case and patient metadata 
    # localhost:8080/cases ==> POST
    # TODO - this has to be modified for batch or bulk upload of cases
    # from device to server
    @endpoints.method(CasesMessage,AstraIDResponseMessage,
        name='astracase.insert',path='cases',http_method='POST')
    def upload_astracase(self, request):
        format_type='%Y-%m-%d'
        patient_date_obj = datetime.strptime(request.patientDOB, format_type).date()
        print("Upload AstraCase:: Client deviceID & caseID::"  + request.deviceID +"::"+ str(request.caseID)) 
        logging.info("Upload AstraCase:: Client deviceID & caseID::"  + request.deviceID +"::"+ str(request.caseID)) 
        
        uploadedcase = Cases(
                        deviceID = request.deviceID, 
                        caseID = request.caseID,
                        patientFirstName = request.patientFirstName,  
                        patientLastName = request.patientLastName,
                        patientDOB = patient_date_obj,
                        patientAddress = request.patientAddress, 
                        patientContactNumber = request.patientContactNumber, 
                        villageName = request.villageName,
                        talukName = request.talukName,
                        districtName = request.districtName,
                        cityName = request.cityName, 
                        stateName = request.stateName,
                        countryName = request.countryName,
                        patientClinicalData = request.patientClinicalDataMsg,
                        patientClinicalHistory = request.patientClinicalHistory,
                        additionalReportHeaderData = request.additionalReportHeaderData,
                        caseAnnotationMetaData = request.caseAnnotationMetaData,
                        caseImageURNPath = request.caseImageURNPath,
                        id =request.deviceID +"-"+ str(request.caseID)
                        )
        uploadedcase_key = None 
        try:
            uploadedcase_key = uploadedcase.put()
            logging.info("Upload AstraCase::Auto generated key for Cases Store is:" + str(uploadedcase_key))
        except Error as notsaved:
            print notsaved
            print("Upload AstraCase::ERROR Saving Case Details into Datastore::"+notsaved) 
            logging.error("Upload AstraCase::ERROR Saving Case Details into Datastore::"+notsaved)
            message = 'ERROR Saving Case Details into Datastore for device "%s" :' % request.caseID
            raise endpoints.BadRequestException(message)   
        if uploadedcase_key is None:
            print("Upload AstraCase::ERROR No Key Generated for Case Info PUT into Datastore::"+request.caseID) 
            logging.error("ERROR No Key Generated for Case Info  PUT into Datastore::"+request.caseID)
            message = 'ERROR No Key Generated for Case Info  PUT into Datastore for device "%s" :' % request.caseID
            raise endpoints.BadRequestException(message)   
        else:
            case_key_id= uploadedcase_key.string_id()
            returnResponse = AstraIDResponseMessage(id=case_key_id)
            logging.info("Upload AstraCase::Storage of case on datastore completed, generated key is:"+case_key_id)
            return returnResponse 
    
    # Invoked on Register of User - Sign Up 
    # localhost:8080/users ==> POST
    @endpoints.method(AstraUserMessage,AstraIDResponseMessage,
        name='astrauser.insert',path='users',http_method='POST')
    def insert_astrauser(self, request):
        format_type='%Y-%m-%d'
        date_of_birth_obj = None
        try:
            date_of_birth_obj = datetime.strptime(request.dateOfBirth, format_type).date()
        except:
            message1 = 'ERROR Incorrect Date of Birth Format, Expected Format:: "%s" ' % format_type
            message2 = '\n Date Sent in request "%s" :' % request.dateOfBirth
            raise endpoints.BadRequestException(message1 + message2)

        logging.debug('Values Received from Client for deviceID:'  + request.astraUserType) 
        registeredUser = None
        astraUserTypeParam = request.astraUserType
        if astraUserTypeParam == "PATHOLOGIST":
            registeredUser = PathologistUser(
                       astraUserEmailAddress = request.astraUserEmailAddress, 
                       astraUserFirstName = request.astraUserFirstName,
                       astraUserLastName = request.astraUserLastName,
                       dateOfBirth = date_of_birth_obj,
                       gender = request.gender,
                       contactNumber = request.contactNumber,
                       address = request.address,
                       pinCode = request.pinCode,
                       cityName = request.cityName,
                       stateName = request.stateName,
                       countryName = request.countryName,
                       isHeadOfPathology = request.isHeadOfPathology,
                       mciNumber = request.mciNumber,
                       yearsOfPractice = request.yearsOfPractice,
                       labnameOrHospitalname = request.labnameOrHospitalname,
                       id = request.astraUserEmailAddress
                       )
        elif astraUserTypeParam == "NGOUSER":
            registeredUser = NGOUser(
                       astraUserEmailAddress = request.astraUserEmailAddress, 
                       astraUserFirstName = request.astraUserFirstName,
                       astraUserLastName = request.astraUserLastName,
                       dateOfBirth = date_of_birth_obj,
                       gender = request.gender,
                       contactNumber = request.contactNumber,
                       address = request.address,
                       pinCode = request.pinCode,
                       cityName = request.cityName,
                       stateName = request.stateName,
                       countryName = request.countryName,
                       ngoRegistrationNumber = request.ngoRegistrationNumber,
                       labnameOrHospitalname = request.labnameOrHospitalname,
                       id = request.astraUserEmailAddress
                       )
        elif astraUserTypeParam == "OTHERUSER":
            registeredUser = OtherUser(
                       astraUserEmailAddress = request.astraUserEmailAddress, 
                       astraUserFirstName = request.astraUserFirstName,
                       astraUserLastName = request.astraUserLastName,
                       dateOfBirth = date_of_birth_obj,
                       gender = request.gender,
                       contactNumber = request.contactNumber,
                       address = request.address,
                       pinCode = request.pinCode,
                       cityName = request.cityName,
                       stateName = request.stateName,
                       countryName = request.countryName,
                       yourRequirements = request.yourRequirements,
                       id = request.astraUserEmailAddress
                       )
        else:
            logging.error("Incorrect UserType in Astra User POST API for adding new user" + astraUserTypeParam)
            message = 'ERROR Incorrect user type in request "%s" :' % astraUserTypeParam
            raise endpoints.BadRequestException(message)
        
        registered_user_key = None
        try:
            registered_user_key = registeredUser.put()
        except Error as notsaved:
            print notsaved
            print("ERROR Saving User Details into Datastore::"+notsaved) 
            logging.error("ERROR Saving User Details into Datastore::"+notsaved)
            message = 'ERROR Saving User Details into Datastore for device "%s" :' % request.astraUserEmailAddress
            raise endpoints.BadRequestException(message)   
        
        if registered_user_key is None:
            print("ERROR No Key Generated for User Info PUT into Datastore::"+request.astraUserEmailAddress) 
            logging.error("ERROR No Key Generated for User Info  PUT into Datastore::"+request.astraUserEmailAddress)
            message = 'ERROR No Key Generated for User Info  PUT into Datastore for device "%s" :' % request.astraUserEmailAddress
            raise endpoints.BadRequestException(message)   
        else:
            print("Registered User key:"+ str(registered_user_key))
            user_key_id = registered_user_key.string_id()
            #logging.info("Auto generated key for AstraUser Store is:" + str(user_key_id))
            logging.debug('generating response for user registration request.....')
            returnResponse = AstraIDResponseMessage(id = user_key_id)    
        return returnResponse 
    
    # Admin Configuring a new VisionX Device
    # localhost:8080/devices ==> POST
    @endpoints.method(AstraDeviceMessage,AstraIDResponseMessage,
        name='astradevice.insert',path='devices',http_method='POST')
    def insert_astradevice(self, request):
        format_type='%Y-%m-%d'
        logging.debug('Device First Time Configured Date::' + request.configuredDate)
        device_configured_date_obj = datetime.strptime(request.configuredDate, format_type).date()
        logging.debug('License Renewed??' + str(request.licenseRenewed))
        license_renewed_date_obj = None
        if request.licenseRenewed == True:
            logging.debug('Licensed Renewed Date:::')
            license_renewed_date_obj = datetime.strptime(request.licenseRenewedDate, format_type).date()
       
        logging.debug('Values Received from Client for deviceID:'  + str(request.deviceID) )
        logging.debug('Values Received from Client for modelNumber:'  + str(request.modelNumber) )
        configuredDevice = AstraDevice(
                        deviceID = request.deviceID, 
                        modelNumber = request.modelNumber,
                        configuredDate = device_configured_date_obj,
                        licenseKey = request.licenseKey, 
                        licenseSubscriptionID = request.licenseSubscriptionID, 
                        licenseRenewed = request.licenseRenewed,
                        licenseRenewedDate = license_renewed_date_obj,
                        accessToken = request.accessToken,
                        secretKey = request.secretKey,
                        status = False,
                        id = request.deviceID
                        ) 
        configuredDevice_key = None
        try:
            configuredDevice_key = configuredDevice.put()
            logging.debug('processing and storage on datastore completed')
            print("Key inserted by Datastore for model AstraDevice  is:" + str(configuredDevice_key))
            logging.info("Key inserted by Datastore for model AstraDevice is:" + str(configuredDevice_key))
        except Error as notsaved:
            print notsaved
            print("ERROR Saving Device Details into Datastore::"+notsaved) 
            logging.error("ERROR Saving Device Details into Datastore::"+notsaved)
            message = 'ERROR Saving Device Details into Datastore for device "%s" :' % request.device_id
            raise endpoints.BadRequestException(message)
        if configuredDevice_key is None:
            print("ERROR No Key Generated for Device Details PUT into Datastore::"+request.device_id) 
            logging.error("ERROR No Key Generated for Device Details PUT into Datastore::"+request.device_id)
            message = 'ERROR No Key Generated for Device Details PUT into Datastore for device "%s" :' % request.device_id
            raise endpoints.BadRequestException(message)
        else:
            print("generating response for POST request for device INSERT for:"+ request.deviceID)
            logging.debug("generating response for POST request for device INSERT for:"+request.deviceID)
            device_key_id = configuredDevice_key.string_id()
            returnResponse = AstraIDResponseMessage(id = device_key_id)    
            return returnResponse

    # Invoked when a PAthologist analyzes a case, annotates and submits a report
    # SUBMIT :: Upload of annotations, upload the report, update case status
    # REFERRAL ::Upload of annotations, upload the report, update case status, 
    #             notify original case owner of REFERRAL_CLOSED 
    # localhost:8080/cases/{astracase_key_id} ==> POST
    POST_CASE_RESOURCE = endpoints.ResourceContainer(AstraCaseUpdatedMessage,
         astracase_id=messages.StringField(1, variant=messages.Variant.STRING))
    @endpoints.method(POST_CASE_RESOURCE,AstraIDResponseMessage,
       name='astracase.submit',path='cases/{astracase_id}',http_method='POST')
    def submit_astracasereport(self, request):
        print("Case ID KEy in Request URL Param is:"+ request.astracase_id)
        logging.debug("Case ID Key in Request URL Param is:"+ request.astracase_id)
        
        case_entity_found = None
	userEmail = None
        try:
            print("Searching in database with key:"+request.astracase_id)
            logging.debug("Searching in database with key:"+request.astracase_id)
            case_entity_found = Cases.get_by_id(request.astracase_id)
        except Error:
            print("ERROR Searching for Case in Datastore with key::"+request.astracase_id) 
            logging.error("ERROR Searching for User in Datastore with key::"+request.astracase_id)
            message = 'Case "%s" not found' % request.astracase_id
            raise endpoints.NotFoundException(message)
        
        if case_entity_found is None:
            print("ERROR Case Not in Datastore::"+request.astracase_id) 
            logging.error("ERROR User Not in Datastore::"+request.astracase_id)
            message = 'Case with the id "%s" Does NOT exist.' % request.astracase_id
            raise endpoints.NotFoundException(message)
        else:
            print("Case Exists in the Datastore with key:"+ request.astracase_id )
            logging.debug("Case Exists in the Datastore with key:"+ request.astracase_id)
            #print(" CASE STATUS STRING::" + request.caseStatus)
            #numeric_case_status = int(request.caseStatus)
    
	    userEmail = case_entity_found.primaryCaseOwner

            if request.updatedSingleAnnotationMetaData:
		logging.debug("Annotation Meta Data exists:"+ str(request.updatedSingleAnnotationMetaData))
                if case_entity_found.updatedAnnotationMetaData is None:
                    case_entity_found.updatedAnnotationMetaData = []
           	    case_entity_found.updatedAnnotationMetaData.append(request.updatedSingleAnnotationMetaData)
	        else:
		    case_entity_found.updatedAnnotationMetaData = []
		    case_entity_found.updatedAnnotationMetaData.append(request.updatedSingleAnnotationMetaData) 
            
	    if case_entity_found.caseReports is None:
                case_entity_found.caseReports = []
        
	    if request.caseSingleReport is not None or not request.caseSingleReport:
		logging.debug("Case Report is prepared by Pathologist")
            	case_entity_found.caseReports.append(request.caseSingleReport)
            
            case_entity_found.caseStatus = request.caseStatus
            
            if CaseStatus(request.caseStatus) == CaseStatus.REFERRED and request.referralPathologistEmailAddress is not None:
               logging.debug("Case is referred for Case ID:"+ request.astracase_id + " to " + request.referralPathologistEmailAddress)
               case_entity_found.caseReferral = True 
               case_entity_found.currentCaseOwner = request.referralPathologistEmailAddress
               logging.debug("Notifying Pathologist " + request.referralPathologistEmailAddress + " for case "+ request.astracase_id) 
               astrautils.send_case_referral_mail(request.referralPathologistEmailAddress)    
            elif CaseStatus(request.caseStatus) == CaseStatus.CLOSED:
                timeOfLastClosedCase = date.today()
                case_entity_found.timeOfLastClosedCase = timeOfLastClosedCase
            elif CaseStatus(request.caseStatus) == CaseStatus.REFERRED_CLOSE:
                case_entity_found.currentCaseOwner = case_entity_found.primaryCaseOwner
                logging.debug("Notifying Pathologist " + request.primaryCaseOwner + " for referral case closure "+ request.astracase_id)
                astrautils.send_case_notify_mail(case_entity_found.primaryCaseOwner)
            else:
                logging.debug("Case still OPEN, maybe saving annotations..")     
            
            try:
		logging.debug("Updating Case entry......")
                updated_case_key = case_entity_found.put()
		logging.debug("Case Entry Updated.....")

		if CaseStatus(request.caseStatus) == CaseStatus.CLOSED: 	
		    # Insert new data into UserCaseClosedMapping where astraUserEmailAddress matches
		    logging.debug("No Closed Cases by this user:" + userEmail)
		    case_closedcases_entity_found = UserCaseClosedMapping(
                                astraUserEmailAddress=userEmail,
                                closedCases=updated_case_key )
		    logging.debug("Adding user-closed case entry..")
		    xxx_key = case_closedcases_entity_found.put()
		    logging.debug("User Case Closed Case Key Value is:"+str(xxx_key))
		    logging.debug("User-Closed Case entry added..")

		    # Remove from existing the UserCaseOpenMapping 
		    query_21=  UserCaseOpenMapping.query()
                    query_22=query_21.filter(UserCaseOpenMapping.astraUserEmailAddress==userEmail)
		    query_23=query_22.filter(UserCaseOpenMapping.allocatedCases==updated_case_key)
		    open_cases_entity = query_23.fetch()
		    if open_cases_entity is not None and len(open_cases_entity)>0:
		        logging.debug("Entry exists in UserCaseOpenMapping exists, need to delete")
			open_cases_entity[0].key.delete()	
			logging.debug("Entry deleted")
		elif CaseStatus(request.caseStatus) == CaseStatus.OPEN:
		    logging.debug("No Change in Staus of Case, Case is still open:" + userEmail)
		elif CaseStatus(request.caseStatus) == CaseStatus.REFERRED:
                    logging.debug("Case Status is Referred:" + userEmail)
		elif CaseStatus(request.caseStatus) == CaseStatus.REFERRED_CLOSE:
                    logging.debug("Case Status is referred close:" + userEmail)
		else:
		    logging.error("Should not enter this loop:" + userEmail)
            except Error as notsaved:
                logging.error("ERROR Updating Case Info in Datastore for case::")
		logging.exception(notsaved)
                message = 'FAILURE of Case Update/Store into Datastore for case "%s" :' % request.astracase_id
                raise endpoints.BadRequestException(message)
        
            if CaseStatus(request.caseStatus) == CaseStatus.CLOSED:
                output_message = "Case is CLOSED"
                returnResponse = AstraIDResponseMessage( id = output_message)
	    elif CaseStatus(request.caseStatus) == CaseStatus.OPEN:
                output_message = "Case is Still OPEN"
                returnResponse = AstraIDResponseMessage( id = output_message)
            elif CaseStatus(request.caseStatus) == CaseStatus.REFERRED:
                output_message = "Case is Referred to " + request.referralPathologistEmailAddress
                returnResponse = AstraIDResponseMessage(id = output_message)
            elif CaseStatus(request.caseStatus) == CaseStatus.REFERRED_CLOSE:
                output_message = "Referred Case is CLOSED by " + request.referralPathologistEmailAddress
                returnResponse = AstraIDResponseMessage(id = output_message)    
            else:
                message = 'INCORRECT Case Status for case "%s", status sent in request %s :' % request.astracase_id % request.caseStatus
                raise endpoints.BadRequestException(message)
         
        return returnResponse
    
    # Invoked when the Configure is invoked from VisionX Device
    # localhost:8080/devices/{device_id} ==> GET
    GET_DEVICEID_RESOURCE = endpoints.ResourceContainer(message_types.VoidMessage,
        device_id=messages.StringField(1, variant=messages.Variant.STRING))
    @endpoints.method(GET_DEVICEID_RESOURCE,AstraDeviceMessage,
        name='astradevice.configure',path='devices/{device_id}',http_method='GET')
    def configure_astradevice(self, request):
        print("ID sent in Request URL Param is:"+ request.device_id)
        logging.debug("ID sent in Request URL Param is:"+ request.device_id)
        #lookup in the database if device is configured by Admin
        # if configured, return the device info to VisioX, so that
        # VisionX can store in its MySQL
        device_entity_found = None
        try:
            print("Searching in database with key:"+request.device_id)
            logging.debug("Searching in database with key:"+request.device_id)
            device_entity_found = AstraDevice.get_by_id(request.device_id)
        except Error:
            print("ERROR Searching for Configured Device in Datastore for device::"+request.device_id) 
            logging.error("ERROR Searching for Configured Device in Datastorefor device::"+request.device_id)
            message = 'Device "%s" not found' % request.device_id
            raise endpoints.NotFoundException(message)
        
        if device_entity_found is None:
            print("ERROR Device Not Configured in Datastore::"+request.device_id) 
            logging.error("ERROR Device Not Configured in Datastore::"+request.device_id)
            message = 'No device with the id "%s" exists.' % request.device_id
            raise endpoints.NotFoundException(message)
        else:
            print("Device is Configured and Available in Datastore::"+request.device_id)
            logging.debug("Device is Configured in Datastore::"+request.device_id)
            licensedRenewedDate_str = None
            if device_entity_found.licenseRenewed == True:
                licensedRenewedDate_str = device_entity_found.licenseRenewedDate.isoformat()
            returnResponse = AstraDeviceMessage( 
                deviceID = device_entity_found.deviceID,
                modelNumber = device_entity_found.modelNumber,
                configuredDate = device_entity_found.configuredDate.isoformat(),
                licenseKey = device_entity_found.licenseKey,
                licenseSubscriptionID = device_entity_found.licenseSubscriptionID,
                licenseRenewed = device_entity_found.licenseRenewed,
                licenseRenewedDate = licensedRenewedDate_str,
                accessToken = device_entity_found.accessToken,
                secretKey = device_entity_found.secretKey,
                status = "false"
            )
            return returnResponse  
         
    # Called after firebase authentication by VisionX Client to check for user exist
    # localhost:8080/users?astrauser_id="" ==> GET ==> lookup_user_exists
    GET_USERID_RESOURCE = endpoints.ResourceContainer(message_types.VoidMessage,
        astrauser_id=messages.StringField(1, variant=messages.Variant.STRING))
    @endpoints.method(GET_USERID_RESOURCE,AstraUserResponseMessage,
        name='astrauser.get',path='users',http_method='GET')
    def lookup_user_exists(self, request):
        print("User ID sent in Request URL Param is:"+ request.astrauser_id)
        logging.debug("User ID sent in Request URL Param is:"+ request.astrauser_id)
        #lookup in the database if user exists 
        user_entity_found = None
        astra_user_type = None
        try:
            print("Searching in database with key:"+request.astrauser_id)
            logging.debug("Searching in database with key:"+request.astrauser_id)
            astra_user_type = "PATHOLOGIST"
            user_entity_found = PathologistUser.get_by_id(request.astrauser_id)
            if user_entity_found is None:
                astra_user_type = "NGOUSER"
                user_entity_found = NGOUser.get_by_id(request.astrauser_id)
            if user_entity_found is None:
                astra_user_type = "OTHERUSER"
                user_entity_found = OtherUser.get_by_id(request.astrauser_id)
        except Error:
            print("ERROR Searching for User in Datastore with email address::"+request.astrauser_id) 
            logging.error("ERROR Searching for User in Datastore with email address::"+request.astrauser_id)
            message = 'User "%s" not found' % request.astrauser_id
            raise endpoints.NotFoundException(message)
        
        if user_entity_found is None:
            print("ERROR User Not Configured in Datastore::"+request.astrauser_id) 
            logging.error("ERROR User Not Configured in Datastore::"+request.astrauser_id)
            message = 'User with the id "%s" Does NOT exist.' % request.astrauser_id
            raise endpoints.NotFoundException(message)
        else:
            print("User Exists in the Database:"+ request.astrauser_id + " of type "+ astra_user_type)
            logging.debug("User Exists in the Database:"+ request.astrauser_id + " of type "+ astra_user_type)
            astraUserFirstName = user_entity_found.astraUserFirstName
            astraUserLastName = user_entity_found.astraUserLastName
            x = astraUserFirstName + " " + astraUserLastName
            logging.debug("User Full Name in the Database:"+x)
            if astra_user_type == "NGOUSER" or astra_user_type == "OTHERUSER":
                returnResponse = AstraUserResponseMessage(id = astra_user_type,
                        astraUserFullName = x)  
            else:
		logging.debug("User is a PATHOLOGIST")
		#allocatedcases_entity_found = UserCaseOpenMapping.get_by_id(request.astrauser_id) 
		query_1 = UserCaseOpenMapping.query()
		query_2 = query_1.filter(UserCaseOpenMapping.astraUserEmailAddress == request.astrauser_id)
		allocatedcases_entity_found = query_2.fetch()
                allocatedCases_var = [] 
		if allocatedcases_entity_found is None:
		    logging.debug("No Case Reports Available Yet")
		else:
		    logging.debug("User has Open Cases")
		    for xx in allocatedcases_entity_found:
			logging.info("XX Case ID (%s):" % xx.allocatedCases)	
			logging.info("XX Case ID (%s):" % xx.allocatedCases.string_id())
			allocatedCases_var.append(xx.allocatedCases.string_id())

		query_11 = UserCaseClosedMapping.query()
		query_12 = query_11.filter(UserCaseClosedMapping.astraUserEmailAddress == request.astrauser_id)
		closedcases_entity_found = query_12.fetch()
		closedCases_var = []
		if closedcases_entity_found is None:
		    logging.debug("No Closed Cases Available for this User")
		else:
		    logging.debug("Closed Cases Available for this User")	
		    for yy in closedcases_entity_found:
			logging.debug("YY Case ID (%s):" % yy.closedCases)
                        logging.debug("YY Case ID (%s):" % yy.closedCases.string_id())
			closedCases_var.append(yy.closedCases.string_id())

                returnResponse = AstraUserResponseMessage(id = astra_user_type,
                        astraUserFullName = request.astrauser_id,
                        allocatedCases = allocatedCases_var,
			completedCases = closedCases_var)                                     
            
        return returnResponse
   
    # Fetch Dashboard Details for each User
    # localhost:8080/users/{user_id} ==> GET
    GET_USERID_RESOURCE = endpoints.ResourceContainer(message_types.VoidMessage,
        astrauser_id=messages.StringField(1, variant=messages.Variant.STRING))
    @endpoints.method(GET_USERID_RESOURCE,CasesMessage,
        name='astrauser.getuserdetails',path='users/{astrauser_id}',http_method='GET')
    def fetch_user_dashboard_details(self, request):
        print("User ID KEy in Request URL Param is:"+ request.astrauser_id)
        logging.debug("Case ID Key in Request URL Param is:"+ request.astrauser_id)
        #lookup in the database if user details exists
        case_entity_found = None
        user_entity_found = None
        astra_user_type = None
        try:
            print("Searching in database with key:"+request.astrauser_id)
            logging.debug("Searching in database with key:"+request.astrauser_id)
            astra_user_type = "PATHOLOGIST"
            user_entity_found = PathologistUser.get_by_id(request.astrauser_id)
            if user_entity_found is None:
                astra_user_type = "NGOUSER"
                user_entity_found = NGOUser.get_by_id(request.astrauser_id)
            if user_entity_found is None:
                astra_user_type = "OTHERUSER"
                user_entity_found = OtherUser.get_by_id(request.astrauser_id)
        except Error:
            print("ERROR Searching for User in Datastore with email address::"+request.astrauser_id)
            logging.error("ERROR Searching for User in Datastore with email address::"+request.astrauser_id)
            message = 'User "%s" not found' % request.astrauser_id
            raise endpoints.NotFoundException(message)

        if user_entity_found is None:
            print("ERROR User Not Configured in Datastore::"+request.astrauser_id)
            logging.error("ERROR User Not Configured in Datastore::"+request.astrauser_id)
            message = 'User with the id "%s" Does NOT exist.' % request.astrauser_id
            raise endpoints.NotFoundException(message)
        else:
            print("User Exists in the Database:"+ request.astrauser_id + " of type "+ astra_user_type)
            logging.debug("User Exists in the Database:"+ request.astrauser_id + " of type "+ astra_user_type)
            astraUserFirstName = user_entity_found.astraUserFirstName
            astraUserLastName = user_entity_found.astraUserLastName
            x = astraUserFirstName + " " + astraUserLastName
            logging.debug("User Full Name in the Database:"+x)
            if astra_user_type == "NGOUSER" or astra_user_type == "OTHERUSER":
                returnResponse = AstraUserResponseMessage(id = astra_user_type,
                        astraUserFullName = x)
            else:
                logging.debug("User is a PATHOLOGIST")
                #allocatedcases_entity_found = UserCaseOpenMapping.get_by_id(request.astrauser_id)
                query_1 = UserCaseOpenMapping.query()
                query_2 = query_1.filter(UserCaseOpenMapping.astraUserEmailAddress == request.astrauser_id)
                allocatedcases_entity_found = query_2.fetch()
                allocatedCases_var = []
                if allocatedcases_entity_found is None:
                    logging.debug("No Case Reports Available Yet")
                else:
                    logging.debug("User has Open Cases")
                    for xx in allocatedcases_entity_found:
                        logging.info("XX Case ID (%s):" % xx.allocatedCases)
                        logging.info("XX Case ID (%s):" % xx.allocatedCases.string_id())
                        allocatedCases_var.append(xx.allocatedCases.string_id())

                query_11 = UserCaseClosedMapping.query()
                query_12 = query_11.filter(UserCaseClosedMapping.astraUserEmailAddress == request.astrauser_id)
                closedcases_entity_found = query_12.fetch()
                closedCases_var = []
                if closedcases_entity_found is None:
                    logging.debug("No Closed Cases Available for this User")
                else:
                    logging.debug("Closed Cases Available for this User")
                    for yy in closedcases_entity_found:
                        logging.debug("YY Case ID (%s):" % yy.closedCases)
                        logging.debug("YY Case ID (%s):" % yy.closedCases.string_id())
                        closedCases_var.append(yy.closedCases.string_id())

                returnResponse = AstraUserResponseMessage(id = astra_user_type,
                        astraUserFullName = request.astrauser_id,
                        allocatedCases = allocatedCases_var,
                        completedCases = closedCases_var)

        return returnResponse

 
    # Fetch Case Details when each Case ID Link is Clicked
    # localhost:8080/cases/{astracase_id} ==> GET
    GET_CASEID_RESOURCE = endpoints.ResourceContainer(message_types.VoidMessage,
        astracase_id=messages.StringField(1, variant=messages.Variant.STRING))
    @endpoints.method(GET_CASEID_RESOURCE,CasesMessage,
        name='astracase.getcase',path='cases/{astracase_id}',http_method='GET')
    def fetch_case_details(self, request):
        print("Case ID KEy in Request URL Param is:"+ request.astracase_id)
        logging.debug("Case ID Key in Request URL Param is:"+ request.astracase_id)
        #lookup in the database if user exists 
        case_entity_found = None
        try:
            print("Searching in database with key:"+request.astracase_id)
            logging.debug("Searching in database with key:"+request.astracase_id)
            case_entity_found = Cases.get_by_id(request.astracase_id)
        except Error:
            print("ERROR Searching for Case in Datastore with key::"+request.astracase_id) 
            logging.error("ERROR Searching for User in Datastore with key::"+request.astracase_id)
            message = 'Case "%s" not found' % request.astracase_id
            raise endpoints.NotFoundException(message)
        
        if case_entity_found is None:
            print("ERROR Case Not in Datastore::"+request.astracase_id) 
            logging.error("ERROR User Not in Datastore::"+request.astracase_id)
            message = 'Case with the id "%s" Does NOT exist.' % request.astracase_id
            raise endpoints.NotFoundException(message)
        else:
            print("Case Exists in the Datastore with key:"+ request.astracase_id )
            logging.debug("Case Exists in the Datastore with key:"+ request.astracase_id)
	    
            villageNameValue = None 
            talukNameValue = None 
            districtNameValue = None 
            cityNameValue = None 
            countryNameValue = None 
            patientClinicalDataMsgValue = None 
            additionalReportHeaderDataValue = None 
            caseAnnotationMetaDataValue = None 
            updatedAnnotationMetaDataValue = None 
            caseImageURNPathValue = None 
            caseReportsValue = None
 
	    if case_entity_found.villageName is not None:
		villageNameValue = case_entity_found.villageName
	    if case_entity_found.talukName is not None:
		talukNameValue = case_entity_found.talukName
	    if case_entity_found.districtName is not None:
		districtNameValue = case_entity_found.districtName 
	    if case_entity_found.cityName is not None:
		cityNameValue = case_entity_found.cityName	
	    if case_entity_found.countryName is not None: 
		 countryNameValue = case_entity_found.countryName
	    if case_entity_found.patientClinicalData is not None: 
            	patientClinicalDataMsgValue = case_entity_found.patientClinicalData
            if case_entity_found.additionalReportHeaderData is not None: 
		additionalReportHeaderDataValue = case_entity_found.additionalReportHeaderData
	    if case_entity_found.caseAnnotationMetaData is not None: 
            	caseAnnotationMetaDataValue = case_entity_found.caseAnnotationMetaData 
	    if case_entity_found.updatedAnnotationMetaData is not None:
            	updatedAnnotationMetaDataValue = case_entity_found.updatedAnnotationMetaData
	    if case_entity_found.caseImageURNPath is not None: 
            	caseImageURNPathValue = case_entity_found.caseImageURNPath
	    if case_entity_found.caseReports is not None: 
            	caseReportsValue = case_entity_found.caseReports	 
	
            returnResponse = CasesMessage(
                                deviceID = case_entity_found.deviceID,
                                caseID = case_entity_found.caseID,
                                patientFirstName = case_entity_found.patientFirstName,
                                patientLastName = case_entity_found.patientLastName,
                                patientDOB = case_entity_found.patientDOB.isoformat(),
                                patientAddress = case_entity_found.patientAddress,
                                patientContactNumber = case_entity_found.patientContactNumber,
                                villageName = villageNameValue,
                                talukName = talukNameValue,
                                districtName = districtNameValue,
                                cityName = cityNameValue,
                                stateName = case_entity_found.stateName,
                                countryName = countryNameValue,
                                patientClinicalDataMsg = patientClinicalDataMsgValue,
                                patientClinicalHistory = case_entity_found.patientClinicalHistory,
                                additionalReportHeaderData = additionalReportHeaderDataValue,
                                caseAnnotationMetaData = caseAnnotationMetaDataValue,
                                caseImageURNPath = caseImageURNPathValue,
                                updatedAnnotationMetaData = updatedAnnotationMetaDataValue,
                                caseReports = caseReportsValue 
                                )    
            return returnResponse

    # Fetch Reports for all Closed Cases for this device 
    # localhost:8080/cases?device_id=""&lastTime="" ==> GET
    GET_DEVICE_CASE_RESOURCE = endpoints.ResourceContainer(message_types.VoidMessage,
        device_id=messages.StringField(1, variant=messages.Variant.STRING),
        lastTime=messages.StringField(2, variant=messages.Variant.STRING))
    @endpoints.method(GET_DEVICE_CASE_RESOURCE,AstraReportResponseMessage,
        name='astracase.reports',path='cases',http_method='GET')
    def fetch_closed_reports(self, request):
        if request.device_id is None or request.lastTime is None:
            message1 = 'ERROR Null Request Parameters.'
            message2 = '\nExpecting 2 Request Parameters device_id and lastTime'
            raise endpoints.BadRequestException(message1+message2)
        if request.device_id == EMPTY_STRING or request.device_id.strip() == EMPTY_STRING:
            request.device_id = None
        if request.lastTime == EMPTY_STRING or request.lastTime.strip() == EMPTY_STRING:
            request.lastTime = None
        if request.device_id is None or request.lastTime is None:
            message1 = 'ERROR Empty Request Parameters.'
            message2 = '\nExpecting Non-Empty Request Parameters for device_id and lastTime'
            raise endpoints.BadRequestException(message1+message2)
            
        print("Device ID Sent in Request:"+ request.device_id)
        logging.debug("Device ID Sent in Request:"+ request.device_id)
        returnResponse = None
        format_type='%Y-%m-%d'
        reqTimeOfLastClosedCase = None

        try:
            reqTimeOfLastClosedCase = datetime.strptime(request.lastTime, format_type).date()
            print("Time of Last Close Case Available with Device:" + str(reqTimeOfLastClosedCase))
            logging.debug("Time of Last Close Case Available with Device:" + str(reqTimeOfLastClosedCase))
        except:
            message1 = 'ERROR Incorrect format for lastTime parameter, Expected Format:: "%s" ' % format_type
            message2 = '\n Parameter Value sent in request "%s" :' % request.lastTime
            raise endpoints.BadRequestException(message1 + message2)
        
        fetched_closed_cases = None
        try:
            print("Searching in database closed cases for device id:"+request.device_id)
            logging.debug("Searching in database closed cases for device id:"+request.device_id)
            query_1 = Cases.query()
            query_2 = query_1.filter(Cases.deviceID == request.device_id)
            query_3 = query_2.filter(Cases.caseStatus == 5)
            query_4 = query_2.filter(Cases.timeOfLastClosedCase > reqTimeOfLastClosedCase)
            fetched_closed_cases = query_3.fetch() 
#             fetched_closed_cases = query_3.fetch(projection=[Cases.deviceID, Cases.caseID, Cases.caseReports]) 
#           fetched_closed_cases = Cases.query(ndb.AND(Cases.deviceID == request.device_id,
#                                     Cases.timeOfLastClosedCase > reqTimeOfLastClosedCase)).fetch(projection=[Cases.deviceID, Cases.caseID, Cases.caseReports]) 
        except Error as datastoreError:
            print("ERROR Searching for Closed Cases in Datastore for device::"+request.device_id) 
            print(datastoreError)
            logging.error("ERROR Searching for Closed Cases in Datastore for device::"+request.device_id)
            logging.error(datastoreError)
            message1 = 'Error When Fetching Closed Cases from Datastore for Device "%s" ' % request.device_id 
            message2 = '\n Server Error "%s" ' % datastoreError
            raise endpoints.InternalServerErrorException(message1 + message2)
        
        print("Completed Query Execution on Datastore device id:"+request.device_id)
        logging.debug("Completed Query Execution on Datastore device id:"+request.device_id)
        
        if fetched_closed_cases is None or len(fetched_closed_cases) == 0:
            print("No Closed Cases Available in Datastore after lastTime" + request.lastTime +" for device::"+request.device_id) 
            logging.error("No Closed Cases Available in Datastore after lastTime " + request.lastTime +" for device::"+request.device_id)
            message = 'No Closed Cases Available Case in Datastore after lastTime "%s" for device "%s" .' % request.lastTime  % request.device_id
            raise endpoints.NotFoundException(message)
        
        print("Case Available after the timeOfLastClosedCase on device::"+ request.device_id )
        logging.debug("Case Available after the timeOfLastClosedCase on device::"+ request.device_id)
        
        closed_case_deviceid = None
        closed_case_ids = []
        closed_case_reports = []
   
        for each_closed_case in fetched_closed_cases:
            closed_case_deviceid = each_closed_case.deviceID
            if each_closed_case.caseReports is not None and len(each_closed_case.caseReports) > 0:
                closed_case_ids.append(str(each_closed_case.caseID))
                closed_case_reports.append(each_closed_case.caseReports[-1]) # last element

        print("Case Available after the timeOfLastClosedCase for device::"+ closed_case_deviceid )
        logging.debug("Case Available after the timeOfLastClosedCase for device::"+ closed_case_deviceid)
        
        if len(closed_case_ids) ==0:
            print("No Closed Cases Available after lastTime" + request.lastTime +" on device::"+request.device_id) 
            logging.error("No Closed Cases Available after lastTime " + request.lastTime+ " on device::"+request.device_id)
            message = 'No Closed Cases Available after lastTime "%s" on device "%s" .' % request.lastTime  % request.device_id
            raise endpoints.NotFoundException(message)
            
        returnResponse = AstraReportResponseMessage(
            deviceid = request.device_id,
            closedCaseIDs = closed_case_ids,
            closedCaseReports = closed_case_reports)
        
        return returnResponse
    
    # Verifying and Activating a Registered User - Invoked by Admin 
    # from Admin Web Portal - Click of Activate Button on UI
    # localhost:8080/users ==> POST
    # http://localhost:8080/_ah/api/cervastraApi/v1/users/{user_id} ==> POST
    #http://localhost:8080/_ah/api/cervastraApi/v1/users/kvsandya@gmail.com
    POST_USER_ACTIVATE_RESOURCE = endpoints.ResourceContainer(message_types.VoidMessage,
        astrauser_id=messages.StringField(1, variant=messages.Variant.STRING))
    @endpoints.method(POST_USER_ACTIVATE_RESOURCE,AstraIDResponseMessage,
        name='astrauser.activate',path='users/{astrauser_id}',http_method='POST')
    def activate_astrauser(self, request):
        print("User ID in Request URL Param is:"+ request.astrauser_id)
        logging.debug("User ID  in Request URL Param is:"+ request.astrauser_id)
        
        user_entity_found = None
        try:
            print("Searching in database with key:"+request.astrauser_id)
            logging.debug("Searching in database with key:"+request.astrauser_id)
            user_entity_found = PathologistUser.get_by_id(request.astrauser_id)
        except Error as e:
            print("ERROR Searching for Pathologist User in Datastore with key::"+request.astrauser_id + " " + e) 
            logging.error("ERROR Searching for Pathologis tUser in Datastore with key::"+request.astrauser_id + " " + e)
            message = 'ERROR Searching for Pathologist User "%s" %s' % request.astrauser_id % e
            raise endpoints.NotFoundException(message)
        
        if user_entity_found is None:
            print("ERROR User Not in Datastore::"+request.astrauser_id) 
            logging.error("ERROR User Not in Datastore::"+request.astrauser_id)
            message = 'Pathologist User with the id "%s" Does NOT exist.' % request.astrauser_id
            raise endpoints.NotFoundException(message)
        else:
            print("Pathologist User Exists in the Datastore with key:"+ request.astrauser_id )
            logging.debug("Pathologist User Exists in the Datastore with key:"+ request.astrauser_id )
            print("Activating Pathologist :"+ request.astrauser_id )
            logging.debug("Activating Pathologist :"+ request.astrauser_id )
            try:
                user_entity_found.userVerfiedStatus = True
                user_entity_found.userActiveStatus = True
                updated_key = user_entity_found.put()
                logging.info("Pathologist User Activation Successful for:" + request.astrauser_id )
            except Error as notupdated:
                print notsaved
                print("ERROR in Pathologist User Activation::"+notupdated) 
                logging.error("ERROR in Pathologist User Activation::"+notupdated)
                message = 'ERROR in Pathologist User Activation "%s" :' % request.astrauser_id
                raise endpoints.BadRequestException(message) 
            returnResponse = AstraIDResponseMessage(id = "ACTIVATED")  
            return returnResponse
    
    #Listing all Registered Users with Type and Activation Status 
    #  - Invoked by Admin from Admin Web Portal
    # localhost:8080/users ==> GET  ==> get_all_users()
    #  http://localhost:8080/_ah/api/cervastraApi/v1/users/all ==> GET ==> fetch_all_users
    GET_ALLUSERS_RESOURCE = endpoints.ResourceContainer(message_types.VoidMessage)
    @endpoints.method(GET_ALLUSERS_RESOURCE,AllAstraUsersResponseMessage,
        name='astrauser.listall',path='users/all',http_method='GET')
    def fetch_all_users(self, request):
        try:
            print("Searching in database with key:"+request.astrauser_id)
            logging.debug("Searching in database with key:"+request.astrauser_id)
            #pathologist_entities = PathologistUser.query().
            #        fetch(projection=[id, astraUserFullName, userVerfiedStatus, userActiveStatus])
            pathologist_entities, prev_b, next_b = return_query_page(PathologistUser, 20, None, None, None)
            
            #calling next page
            #pathologist_entities, prev_b, next_b = return_query_page(PathologistUser, 20, next_b, None, None)
    
            #calling prev page
            #pathologist_entities, prev_b, next_b = return_query_page(PathologistUser, 20, prev_b, True, None)
    
        except Error as e:
            print("ERROR Searching for Pathologist User in Datastore with key::"+request.astrauser_id + " " + e) 
            logging.error("ERROR Searching for Pathologis tUser in Datastore with key::"+request.astrauser_id + " " + e)
            message = 'ERROR Searching for Pathologist User "%s" %s' % request.astrauser_id % e
            raise endpoints.NotFoundException(message)
        
        if user_entity_found is None:
            print("ERROR User Not in Datastore::"+request.astrauser_id) 
            logging.error("ERROR User Not in Datastore::"+request.astrauser_id)
            message = 'Pathologist User with the id "%s" Does NOT exist.' % request.astrauser_id
            raise endpoints.NotFoundException(message)
        else:
            print
            
        pass 
    
# [START api_server]
api = endpoints.api_server([CervastraApi])
# [END api_server]
    
