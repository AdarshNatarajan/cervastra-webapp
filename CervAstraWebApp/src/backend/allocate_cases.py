'''
Created on 08-Apr-2018

@author: shobhasrao
  Build and Execute a strategy to fetch all open Cases and 
  assign to active Pathologist USers - once a day called via CRON
  Also notify Pathologist via email and/or SMS after allocation
  Temporary strategy 1 case to each active pathologist , and repeat across all cases
  and all active pathologist
'''
import logging
from astra_models_v1 import Cases
from astra_models_v1 import UserCaseOpenMapping
from astra_models_v1 import PathologistUser
from google.appengine.ext.db import Error
from google.appengine.ext import ndb
import endpoints
import itertools

class AllocationError(Exception):
    def __init__(self, value): 
        self.value = value 
  
    def __str__(self): 
        return(repr(self.value)) 

def fetch_new_cases_from_datastore():
    fetched_new_case_ids = []  
    fetched_new_cases = []
    try:
        logging.debug("Fetching all NEW Cases..")
        #fetched_new_case_ids = Cases.query(Cases.caseStatus == 1).order().fetch(projection = [caseID]) 
        query_1 = Cases.query()
        query_3 = query_1.filter(Cases.caseStatus == 1)
        fetched_new_cases = query_3.fetch()
        if fetched_new_cases is None:
            logging.debug("No New Cases Available for Allocation..")
            logging.error("No NEW Cases Available that require Allocation")
            return None
        else:
            logging.debug("Cases are available for Allocation, Start Allocation...")
            logging.debug(len(fetched_new_cases))
            for eachcase in fetched_new_cases:
                case_id=eachcase.key
                fetched_new_case_ids.append(case_id)
                logging.debug("Case ID Key is:" + str(case_id))
            
            return fetched_new_case_ids
    except Error as serverError:
        logging.error("ERROR Fetching  all NEW Cases from Datastore::" + str(serverError))
        message = 'ERROR Fetching  all NEW Cases from Datastore with Error "%s" ' % serverError
        raise AllocationError(message)
        
        
def fetch_active_pathologists_from_datastore():
    fetched_all_active_pathologists_ids = []
    fetched_path_users = []
    try:
        logging.debug("Fetching all Active Pathologists..")
        pquery_1 = PathologistUser.query()
        #pquery_2 = pquery_1.filter(PathologistUser.userActiveStatus.IN([True]))
        #pquery_2 = pquery_1.filter(PathologistUser.userActiveStatus.IN(['true']))
        #fetched_path_users = pquery_2.fetch()
        fetched_path_users = pquery_1.fetch()
        if fetched_path_users is None:
            logging.debug("No Active Pathologists Available, Register New Pathologists OR Activate Pathologists!!")
            return None
        else:
            for eachuser in fetched_path_users:
                user_id =  eachuser.key #This is the entity key
                if eachuser.userActiveStatus==True:
                    fetched_all_active_pathologists_ids.append(user_id)
                    logging.debug("Active Pathologist ID:"+str(user_id) + ":Status:"+ str(eachuser.userActiveStatus))
            
            return fetched_all_active_pathologists_ids  
        
    except Error as datastoreError:
        logging.error("ERROR Fetching all Active Pathologists" + str(datastoreError))
        message = 'Fetching all Active Pathologists with Error "%s" ' % datastoreError
        raise AllocationError(message)

    
    
def izip_longest_repeat(*args):
    if args:
        lists = sorted(args, key=len, reverse=True)
        result = list(itertools.izip(*([lists[0]] + [itertools.cycle(l) for l in lists[1:]])))
    else:
        result = [()]
    return result

def assign_cases_to_pathologist(case_list, patho_list):
    return_list= None
    if len(case_list) < len(patho_list):
        return_list = list(itertools.izip_longest(case_list, patho_list))
    elif len(case_list) == len(patho_list):
        return_list = list(itertools.izip_longest(case_list, patho_list))
    else:
        return_list = izip_longest_repeat(case_list, patho_list)
    
    return return_list

@ndb.transactional(xg=True)
def update_user_case_mapping_store(case_id, pathologist_id):
    try:
        logging.debug("Map Case to Pathologists......." + str(case_id))
        logging.debug(str(pathologist_id))
        pathologistObject = pathologist_id.get() # Get the object using key
        logging.debug(pathologistObject.astraUserEmailAddress)
        emailAddress = pathologistObject.astraUserEmailAddress
        logging.debug(emailAddress) 
	caseObject = case_id.get() # Get the Case Object using key
        logging.debug("Retreived Case object for case id " + str(case_id))

	if caseObject is not None and caseObject.caseStatus is not None:
		logging.debug("Data available..")
		if caseObject.caseStatus!=2 and caseObject.caseStatus==1: 
        		caseObject.caseStatus = 2
        		caseObject.primaryCaseOwner = str(emailAddress)
        		caseObject.currentCaseOwner = str(emailAddress)
        		update_case_key = caseObject.put()
			key_string_id= update_case_key.string_id()
			logging.debug("Key String id is:"+key_string_id)
        		openusercasesmapping = UserCaseOpenMapping(
        			astraUserEmailAddress=emailAddress,  
    				allocatedCases=update_case_key  )
        		openusercasesmapping.put()
        		logging.info("UserCaseMapping insert completed.....")
		else:
			logging.debug("This Case is Not a NEw Case!!!, do not allocate this case")
	else:
		logging.debug("Case is None or CaseStatus is None!!!")

    except AllocationError as e:
        message = 'ERROR No Key Generated for User Info  PUT into Datastore for device "%s" :' % request.astraUserEmailAddress
        message = "ERROR When allocating each case to a user for %s For Pathologist %s" % case_id, pathologist_id
        logging.error("ERROR When allocating each case to a user for " + case_id + " For Pathologist " + pathologist_id)
        raise AllocationError(message)
    except Exception as e1:
        logging.error("Unexpected Exception!!")
        logging.error(e1)
        message = 'Unexpected Exception!! %s ' %e1.message
        raise AllocationError(message)
    except Error as e2:
        logging.error("Unexpected Error!!")
        logging.error(e2)
        message = 'Unexpected Exception!! %s ' %e2.message
        raise AllocationError(message)

@ndb.transactional(xg=True)
def update_case_pathologist_store(case_id, pathologist_id):
    try:
        logging.debug("Map Case to Pathologists......." + str(case_id))
	logging.debug(str(pathologist_id))
	pathologistObject = pathologist_id.get() # Get the object using key
	logging.debug(pathologistObject.astraUserEmailAddress)
	emailAddress = pathologistObject.astraUserEmailAddress
	logging.debug(emailAddress)
        caseObject = case_id.get() # Get the Case Object using key
        logging.debug("Retreived Case object for case id " + str(case_id))

        caseObject.CaseStatus = 2
        caseObject.primaryCaseOwner = str(emailAddress)
        caseObject.currentCaseOwner = str(emailAddress) 
        update_case_key = caseObject.put()
        logging.debug("Updated Case  " + str(case_id))
        if pathologistObject.allocatedCases is None:
	    key_string_id= update_case_key.string_id()
            logging.debug("KEy is " + key_string_id)
            #pathologistObject.allocatedCases = [ update_case_key ]
            pathologistObject.allocatedCases = [ key_string_id ]
	else:
            key_string_id= update_case_key.string_id()
            logging.debug("KEy is " + key_string_id) 
	    #pathologistObject.allocatedCases.append(update_case_key)
	    pathologistObject.allocatedCases.append(key_string_id)

        pathologistObject.put()
        logging.debug("Updated Pathologist User with allocated Case" + str(pathologist_id))
    except AllocationError as e:
        message = 'ERROR No Key Generated for User Info  PUT into Datastore for device "%s" :' % request.astraUserEmailAddress
        message = "ERROR When allocating each case to a user for %s For Pathologist %s" % case_id, pathologist_id
        logging.error("ERROR When allocating each case to a user for " + case_id + " For Pathologist " + pathologist_id)
        raise AllocationError(message)
    except Exception as e1:
	logging.error("Unexpected Exception!!")
        logging.error(e1)
        message = 'Unexpected Exception!! %s ' %e1.message
        raise AllocationError(message)
    except Error as e2:
        logging.error("Unexpected Error!!")
        logging.error(e2)
        message = 'Unexpected Exception!! %s ' %e2.message
        raise AllocationError(message)

def allocationStrategy1():
    resultFlag=""
    saveErrorList = [] 
    fetched_new_cases =[]
    fetched_all_active_pathologists=[]
    resp = None
    result_list = []
    try:
       # print("This is Allocation Strategy of Simple First Come First Server Basis:"+self.name )
       fetched_new_cases  = fetch_new_cases_from_datastore()
       if fetched_new_cases is None or len(fetched_new_cases)==0:
           logging.debug("No New  Cases to Assign to Users....") 
           resp='200'
           return resp
       fetched_all_active_pathologists  = fetch_active_pathologists_from_datastore()
       if fetched_all_active_pathologists is None or len(fetched_all_active_pathologists)==0:
           logging.debug("No Active Users to Assign Cases....") 
           resp='200'
           return resp
       #fetched_new_cases, fetched_all_active_pathologists  = fetch_assign_new_cases_from_datastore()
    except AllocationError  as e:
        logging.error("ERROR in DATATORE READ OPERATION!!!!")
        logging.error(e)
        resp='500'
        return resp
    
    logging.debug("Assign Cases to Users....") 
    result_list = assign_cases_to_pathologist(fetched_new_cases, fetched_all_active_pathologists)
    logging.debug("Completed Assigning Cases to Users....") 
 
    if result_list is None or len(result_list)==0:
        logging.error("Active Users or New Cases is None, No Allocation required.....")
        resp= '500'
        return resp
     
    for eachitem in result_list:
        logging.debug("Assigning Each Case to Pathologist...")
        case_id, pathologist_id = eachitem
        if case_id is not None:
            try:
		update_user_case_mapping_store(case_id, pathologist_id)
            except AllocationError as saveError:
                print("ERROR when Assigning Each Case to Pathologist::"+ str(case_id) + "::" + str(pathologist_id))
                logging.error("ERROR when Assigning Each Case to Pathologist.::"+ case_id + "::" + pathologist_id)
                saveErrorList.append(saveError)
                continue
                
    logging.debug("Allocation of Users to Cases completed......")    
    if saveErrorList is not None and len(saveErrorList) >0:
        logging.error("Errors Observed During Allocation of Users to Cases!!!")   
        resp='500'
        return resp   
    else:
       resp='200'  
       return resp                          

    
class AllocationStrategy:
    
    def __init__(self, name, func=None):
        self.name = name
        self.strategy = func

    def executeStrategy(self):
        return self.strategy()

            

# HOW TO INVOKE
#allocateStrat = AllocationStrategy(allocationStrategy1)
#allocateStrat.name = 'Simple First Come First Server Basis'
#saveErrorList = allocateStrat.allocate()
#if saveErrorList is not None and len(saveErrorList> >0:
