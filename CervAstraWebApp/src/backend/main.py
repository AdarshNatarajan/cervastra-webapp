'''
Created on 29-Mar-2018

@author: shobhasrao
'''
import logging
from google.appengine.ext import ndb
import webapp2
from astra_models_v1 import PathologistUser 
from allocate_cases import AllocationStrategy
from allocate_cases import *

import os
import jinja2


# [START add_entities]
class AddEntitiesHandler(webapp2.RequestHandler):
    """Adds new entities using the v1 schema."""
    def post(self):
        # Force ndb to use v1 of the model by re-loading it.
        reload(astra_models_v1)
        # Save some default data.
        logging.debug('Loading default data for subscriptions')
        ndb.put_multi([
            astra_models_v1.AstraSubscription(id="subscription1", subscriptionID=1, 
                subscriptionName='monthly',subscriptionPriceINR=50000,
                subscriptionAllottedRunsCount=100,subscriptionValidityInDays=30),
            astra_models_v1.AstraSubscription(id="subscription2", subscriptionID=2, 
                subscriptionName='quarterly',subscriptionPriceINR=100000,
                subscriptionAllottedRunsCount=300,subscriptionValidityInDays=90),
            astra_models_v1.AstraSubscription(id="subscription3", subscriptionID=3, 
                subscriptionName='half_yearly',subscriptionPriceINR=150000,
                subscriptionAllottedRunsCount=600,subscriptionValidityInDays=180),
            astra_models_v1.AstraSubscription(id="subscription4", subscriptionID=4, 
                subscriptionName='yearly',subscriptionPriceINR=200000,
                subscriptionAllottedRunsCount=1200,subscriptionValidityInDays=360),
            astra_models_v1.AstraSubscription(id="subscription5",subscriptionID=5, 
                subscriptionName='demo',subscriptionPriceINR=0,
                subscriptionAllottedRunsCount=15,subscriptionValidityInDays=5)
        ])
        logging.debug('Completed Loading default data for subscriptions')
        #self.response.
        
class CursorPaginationHandler(webapp2.RequestHandler):
    """Display objects with cursor pagination"""

    def get(self):
        prev_cursor = self.request.get('prev_cursor', '')
        next_cursor = self.request.get('next_cursor', '')
        res = PathologistUser.cursor_pagination(prev_cursor, next_cursor)
        template = JINJA_ENVIRONMENT.get_template('templates/cursor_pagination.html')
        self.response.write(template.render(res))        
        
class ScheduledCronHandler(webapp2.RequestHandler):
    
    def get(self):
        allocateStrat = AllocationStrategy('Simple First Come First Serve Allocation', allocationStrategy1)
        response  = allocateStrat.executeStrategy()
        if response == '500':
           logging.error("ERROR/(s) in Scheduled CRON JOB")
        elif response == '200':
            logging.debug("SUCCESS in Allocating Cases to Users ")
        else:
            logging.error("ERROR!!, Unknown Error!!!")
            
                                
# [END add_entities]
#  ('/wwwastra/cursor_pagination', CursorPaginationHandler),
#    ('/wwwastra/add_entities', AddEntitiesHandler),
app = webapp2.WSGIApplication([
    ('/wwwastra/scheduled_cron_job', ScheduledCronHandler),
    ], debug= True)
