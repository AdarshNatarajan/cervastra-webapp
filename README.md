### CERVASTRA WEBAPP ###
This repository represents the source for the CervAsra Web Application Server.

### Getting Started ###
These instructions will get you a copy of the project up and running on your local machine 
for development and testing purposes. 
See deployment for notes on how to deploy the project on a production system.

### Google App Engine Installation and Set Up ###
Create a Google Account
Login to Google Cloud Platform (GCP) Console using google account
Create a project on GCP Console, Go to Appengine and create a new project [projectname-xyzabc]
Create a Google Datastore Owner Account

### Firebase Set Up ###
To add Firebase to your app, you'll need a Firebase project, the Firebase SDK, 
and a short snippet of initialization code that has a few details about your project.
Create a Firebase project in the Firebase console: https://console.firebase.google.com
and mention the GoogleAppEngine project name

### Local Machine Installation and Set Up ###
On a linux termimal, Install and initialize Google Cloud SDK:
     * Download and install GoogleAppEngine SDK for Python (Standard Environment)
     * https://cloud.google.com/appengine/downloads#Google_App_Engine_SDK_for_Python
     * Extract the tar file
     * Install the SDK
		linux>  ./google-cloud-sdk/install.sh
	 * Initialize the SDK
		linux>  ./google-cloud-sdk/bin/gcloud init
			the final output should display this:
			Your current project has been set to: [projectname-xyzabc].

On a linux terminal, clone this repository
    linux> cd CervAstra/src/backend
	linux> pip install -r requirements.txt -t lib
	linux> pip install --upgrade google-api-python-client
	linux> gcloud auth login
	linux> gcloud config set project [projectname-xyzabc]
    linux> python lib/endpoints/endpointscfg.py get_openapi_spec cervastraapi.CervastraApi --hostname [projectname-xyzabc].appspot.com
	       If you face any issues in running this endpintscfg.py, then go to lib folder and execute:
		   linux> chmod +x lib/endpoints

## GAE Deploy Instructions ##
To Deploy the Google Cloud Endpoints:
	linux> gcloud endpoints services deploy cervastraApiv1openapi.json
            Output of the above linux command ends with your current date and a release version:
			For Example:
			Service Configuration [2018-03-14r0] uploaded for service [projectname-xyzabc]
	linux> Edit the app.yaml with :
			ENDPOINTS_SERVICE_NAME: [projectname-xyzabc].appspot.com
  			ENDPOINTS_SERVICE_VERSION: 2018-03-14r0
	The Google Cloud Endpoints are Now deployed on cloud

You can NOW test your applciation locally OR on the GoogleAppEngine 
## For Local Testing ##
   linux> dev_appserver.py ./app.yaml
REST API URL examples used by Postman or VisionX Python Client are:
 		POST ==> http://localhost:8080/_ah/api/cervastraApi/v1/devices
		GET ==> http://localhost:8080/_ah/api/cervastraApi/v1/devices?device_id=VX12AB457
 
## For Google Cloud Testing ##
   linux> gcloud app deploy
You can test your Cloud endpoints using Postman OR  VisionX Python Client
NOTE: The above test uses the Google Cloud Datastore 
REST API URL examples used by Postman or VisionX Python Client are:
 		POST ==> http://[projectname-xyzabc].appspot.com/_ah/api/cervastraApi/v1/devices
		GET ==> http://[projectname-xyzabc].appspot.com/_ah/api/cervastraApi/v1/devices?device_id=VX12AB457
 		POST ==> https://[projectname-xyzabc].appspot.com/_ah/api/cervastraApi/v1/cases

## Cleanup After testing ##
linux> gcloud endpoints services delete <my-service>
linux> gcloud app versions list
linux> gcloud app versions delete <v1> <v2>
	
### Who do I talk to? ###
Repository Owner: Adarsh Natarajan
Repository Initial Author: shobhasrao